
const path = require('path');
const cors = require('cors');
const express = require('express');
const sequelize =require('sequelize');


const app = express();
app.use(cors("http://localhost:3000"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
var bodyparser = require('body-parser');
app.use(bodyparser.urlencoded({ extended: true }));



const RoleRoutes = require('./Routes/RoleRoutes')
app.use('/roles', RoleRoutes)


const userRoutes = require('./Routes/userRoutes')
app.use('/user',userRoutes)


app.listen(9000,() => console.log('srever start'));

