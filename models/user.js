const Sequelize = require('sequelize');
const sequelize = require('../config/db');
const roles = require('./Role');

const user = sequelize.define('user', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    firstname: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    lastname: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    emailVerified: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: 0,
      },
    emailToken: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    phonenumber: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false,
    },
    status: {
        type: Sequelize.ENUM,
        values: ['active', 'inactive', 'trash'],
        defaultValue: 'active',
    },
    deletedAt: {
        type: Sequelize.DATE, 
        allowNull: true,
    },
    role_id:{
        type: Sequelize.INTEGER,
         defaultValue:3,
        allowNull: false,
       references: {
        model: "roles",
        key:"id"
        }
    }
}, {
    freezeTableName: true,
    timestamps: true, 
    paranoid: true, 
    hooks: {
        beforeValidate: (user) => {
            if (user.firstname) {
                user.firstname = user.firstname.trim();
            }
            if (user.lastname) {
                user.lastname = user.lastname.trim();
            }
            if (user.email) {
                user.email = user.email.trim();
            }
            if (user.phonenumber) {
                user.phonenumber = user.phonenumber.trim();
            }
        }
    }
});

user.sync()
    .then(() => {
        console.log('Table created successfully');
    })
    .catch((e) => {
        console.error('Error creating table', e);
    });
       roles.hasMany(user, { foreignKey: 'role_id' });
       user.belongsTo(roles, { foreignKey: 'role_id' });


module.exports = user;
