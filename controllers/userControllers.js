//const { where } = require('sequelize');
const jwt = require('jsonwebtoken');
const sequelize = require('../config/db');
const user = require('../models/user')
const { Op, where } = require('sequelize');
const Role = require('../models/Role');
const nodemailer = require("nodemailer");
const crypto = require('crypto');

// const roles = require('../models/Role');

//-------------------creste user--------------------


// const addUser = async (req, res) => {
//     try {
//         const requiredFields = [
//             "firstname",
//             "email",
//             "phonenumber",
//             "password",
//             // "role_id",
//         ];

//         requiredFields.forEach(value => {
//             if (!req.body[value] || req.body[value].trim() =="") {
//                 return res.status(400).json({ error: `${value}Field is Required` });
//             }
//         });
//         const roleExist = await Role.findOne({
//             where: { id: req.body.role_id }
//         });
//         // console.log("helooi")
//         if (!roleExist) {
//             return res.status(400).json({ error: "Role not found" })
//         }

//         const PhoneNumberExist = await user.findOne({
//             where: { phonenumber: req.body.phonenumber }
//         });
//         if (PhoneNumberExist) {
//             return res.status(400).json({ error: "User exist with the same number try another" })
//         }
//         const EmailExist = await user.findOne({
//             where: { email: req.body.email }
//         });

//         if (EmailExist) {
//             return res.status(400).json({ error: "User exist with the same email try another" });
//         }

//         const data = await user.create({
//             firstname: req.body.firstname,
//             lastname: req.body.lastname,
//             email: req.body.email,
//             phonenumber: req.body.phonenumber,
//             password: req.body.password,
//             role_id:req.body.role_id,

//         })

//         res.status(200).json(data)
//     } catch (err) {
//         console.log(err)
//         res.status(500).json({ message: "Some database error occurred", error: err })
//     }
// }
//-----------------------mail------------------------

const addUser = async (req, res) => {
    try {
        const requiredFields = [
            "firstname",
            "email",
            "phonenumber",
            "password",
           
        ];

        requiredFields.forEach(value => {
            if (!req.body[value] || req.body[value].trim() == "") {
                return res.status(400).json({ error: `${value}Field is Required` });
            }
        });
        const roleExist = await Role.findOne({
            where: { id: req.body.role_id }
        });
        // console.log("helooi")
        if (!roleExist) {
            return res.status(400).json({ error: "Role not found" })
        }

        const PhoneNumberExist = await user.findOne({
            where: { phonenumber: req.body.phonenumber }
        });
        if (PhoneNumberExist) {
            return res.status(400).json({ error: "User exist with the same number try another" })
        }
        const EmailExist = await user.findOne({
            where: { email: req.body.email }
        });

        if (EmailExist) {
            return res.status(400).json({ error: "User exist with the same email try another" });
        }

        function generateRandomToken(length = 15) {
            return crypto.randomBytes(Math.ceil(length / 2))
                .toString('hex')
                .slice(0, length);
        }
        const token = generateRandomToken();
        console.log(token);

        const data = await user.create({
            firstname: req.body.firstname,
            lastname: req.body.lastname,
            email: req.body.email,
            phonenumber: req.body.phonenumber,
            password: req.body.password,
            role_id: req.body.role_id,
            emailToken: token,
            // emailVerified:false

        })

        if (data) {
            const transporter = nodemailer.createTransport({
                host: 'smtp.gmail.com',
                port: 587,
                secure: false,
                auth: {
                    user: 'amal.shibu.tblr@gmail.com',
                    pass: 'mdtjnwxpxjhelzie',
                },
            });
            console.log("transporter")

            const mailOptions = {
                from: 'amal.shibu.tblr@gmail.com',
                to: req.body.email,
                subject: 'Welcome to Our Website',
                html: `<p>Thank you for registering on our website! Your account has been created. 
                    Click <a href="http://localhost:9000/user/emailVerify/${data.id}?token=${token}">here</a> to visit our website.</p>`,
            };
            console.log("mailOptions")

            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                } else {
                    console.log('Confirmation email sent: ' + info.response);
                }
            });


            res.status(200).json(data);
        } else {
            res.status(400).json({ Error: 'Error in inserting new record' });
        }
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};




//------------updateUser-----------

//  const updateUser = async (req, res) => {
//      try{

//         const id = req.params.id;
//         const updatedUser = await user.update({
//             firstname: req.body.firstname,
//             lastname: req.body.lastname,
//             email: req.body.email,
//             phonenumber: req.body.phonenumber,
//             password: req.body.password,

//         }, {
//             where: { id: id },
//         });


// if (updatedUser) {
//         res.status(200).json({ message: 'User updated successfully' });
//     } else {
//         res.status(404).json({ error: 'User not found' });
//     }

// } catch (error) {
//     res.status(500).json({ error: 'Error updating user' });
// }
//  }


const updateUser = async (req, res) => {
    try {
        console.log("update user...................")
        const id = req.params.id;
        const keys = Object.keys(req.body);



        if (keys.length === 0) {
            return res.status(400).json({ error: 'No fields to update' });
        }


        for (const key of keys) {
            if (req.body[key] === "") {
                return res.status(400).json({ error: `${key} is empty` });
            }
        }
        if (req.body.phonenumber) {
            const PhoneNumberExist = await user.findOne({
                where: { phonenumber: req.body.phonenumber }
            });
            if (PhoneNumberExist) {
                return res.status(400).json({ error: "User exists with the same phone number try another" });
            }
        }
        if (req.body.email) {
            const EmailExist = await user.findOne({
                where: { email: req.body.email }
            });

            if (EmailExist) {
                return res.status(400).json({ error: "User exists with the same email try another" });
            }
        }
        const [updated] = await user.update(req.body, {
            where: { id: id },
        });

        if (updated) {
            res.status(200).json({ message: 'User updated successfully' });
        } else {
            res.status(404).json({ error: 'User not found' });
        }
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Error updating user' });
    }
};

// -----------------Delete user-----------------
const deletedAt = async (req, res) => {
    try {
        const id = req.params.id;
        const [deletedAt] = await user.update({ status: 'trash' }, {
            where: {
                id: id
            },
        });
        //  console.log(deletedAt,"user")
        if (deletedAt) {
            res.status(200).json({ message: 'User deleted successfully' });
        } else {
            res.status(404).json({ error: 'User not found' });
        }
    } catch (error) {
        res.status(500).json({ error: 'Error deleting user' });
    }
};




//---------------------List User--------------------


const getUsers = async (req, res) => {
    try {
        const usersList = await user.findAll(
            {
                where: {
                    status: {
                        [Op.ne]: 'trash'
                    }
                },
                include: [{
                    model: Role,
                    attributes: ['role_name'],
                    required: true,
                }]
            });


        res.status(200).json(usersList);
    } catch (error) {
        res.status(500).json({ error: "Can't retrieve users" });
    }
};

//----------------------List by id--------------

const getUserById = async (req, res) => {
    try {
        const id = req.params.id;
        const userById = await user.findOne({
            where: {
                id: id,
                status: {
                    [Op.ne]: 'trash'
                }
            }

        });

        if (userById) {
            res.status(200).json(userById);
        } else {
            res.status(404).json({ error: 'User not found' });
        }
    } catch (error) {
        res.status(500).json({ error: 'Error retrieving user' });
    }
};



const emailVerification = async (req, res) => {
    try {
        console.log("update user....")
    
        const id = req.params.id;

        console.log(req.query.token, "token--------------", typeof (id), "---------", id)
        
     const userRecord = await user.findOne({ where: { id } });
        
        if (!userRecord) {
            return res.status(404).json({ error: 'User not found' });
        }

    
        if (req.query.token !== userRecord.emailToken) {
            return res.status(400).json({ error: 'Invalid or expired token' });
        }

        const [updated] = await user.update({emailVerified: true} , {
            where: { id: id },
        });
       
        if (updated) {
            res.status(200).json({ message: 'User updated successfully' });
        } else {
            res.status(404).json({ error: 'User not found' });
        }
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Error updating user' });
    }
};


module.exports = {
    addUser,
    updateUser,
    deletedAt,
    getUsers,
    getUserById,
    emailVerification,


}