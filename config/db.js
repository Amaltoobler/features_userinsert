const Sequelize = require('sequelize')

const sequelize = new Sequelize('Cruddb','root','password',{
    port: '3306',
    dialect: 'mysql',
    host: 'localhost'
});
sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch((err) => {
        console.log('Unable to connect to the database:', err);
    });

module.exports = sequelize
