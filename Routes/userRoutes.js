
const router = require('express').Router();
const userControllers = require('../controllers/userControllers');
router.post('/addUser',userControllers.addUser);
router.put('/updateUser/:id',userControllers.updateUser);
router.put('/emailVerify/:id',userControllers.emailVerification);
router.post('/deleteAt/:id',userControllers.deletedAt);
router.get('/getUsers',userControllers.getUsers);
router.get('/getUserById/:id', userControllers.getUserById);

module.exports = router