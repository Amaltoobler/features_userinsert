const RoleControllers = require('../controllers/RoleControllers');
const router = require('express').Router();

router.post('/addroles', RoleControllers.addRoles);
router.get('/getroles', RoleControllers.getRoles);


module.exports = router